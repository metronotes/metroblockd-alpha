metroblockd
==============

Provides extended API functionality over metronotesd (such as market information, asset history, etc). Works alongside metronotesd.
